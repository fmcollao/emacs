;;Cargo Theme
(load-theme 'wombat)

;;Font
(add-to-list 'default-frame-alist
                       '(font . "Fantasque Sans Mono-12"))

;;; Iluminar los paréntesis y las llaves por parejas para evitar olvidos:
(show-paren-mode 1)

;;; No hacer copias de seguridad ni crear archivos #auto-save#
(setq make-backup-files nil)
(setq auto-save-default nil)

;;; Mostrar fecha y hora en formato 24 horas:
(setq display-time-day-and-date t
display-time-24hr-format t)
(display-time)

;;; Sin mensaje de bienvenida:
(setq inhibit-startup-message t)
(setq initial-scratch-message nil)

;;; No mostrar la barra del menú:
(menu-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(scroll-bar-mode -1)
(global-hl-line-mode +1)
(column-number-mode 1)
(display-time-mode 1)

;;; Reemplazar "yes" y "no" por "y" y "n"
(fset 'yes-or-no-p 'y-or-n-p)

;;; Mover a la papelera al borrar archivos y directorios:
(setq delete-by-moving-to-trash t
trash-directory "~/.local/share/Trash/files")

;;; guardar la sessión al cerrar emacs y restaurarla
;;; al arrancar-la de nuevo. Cero (0) para desactivar:
(desktop-save-mode 0)

;;; Para que se muestren todos los buffers abiertos al
;;; pulsar C-x b (ido)
(ido-mode 1)
;;; Ignorar determinados buffers.
(setq ido-ignore-buffers '("^ " "*Completions*"
                           "*Shell Command Output*"
                           "*Messages*" "Async Shell Command"
                           "*scratch*" "*tramp*"))

;;UTF-8
(prefer-coding-system 'utf-8)
(set-default-coding-systems 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(if (boundp 'buffer-file-coding-system)
(setq-default buffer-file-coding-system 'utf-8)
(setq default-buffer-file-coding-system 'utf-8))
(setq x-select-request-type '(UTF8_STRING COMPOUND_TEXT TEXT STRING))

;;Keybindings ORG-MODE
(global-set-key "\C-cl" 'org-store-link)
(global-set-key "\C-ca" 'org-agenda)
(global-set-key "\C-cc" 'org-capture)
(global-set-key "\C-cb" 'org-iswitchb)

;;Agenda files
(setq org-agenda-files '
	("~/gtd/inbox.org"
	 "~/gtd/projects.org"
	 "~/gtd/calendar.org"))

;;Capture
(setq org-capture-templates '(("t" "Todo [inbox]" entry
                               (file+headline "~/gtd/inbox.org" "Tareas")
                               "* TODO %i%?")
                              ("T" "Calendar" entry
                               (file+headline "~/gtd/calendar.org" "Calendario")
                               "* %i%? \n %U")))

;;KEYWORDS
(setq org-todo-keywords '
      ((sequence "TODO(t)" "SIGUIENTE(s)" "EN ESPERA(w)" "PROYECTO(p)" "|" "HECHO(d)")))

;;Colors
(setq org-todo-keyword-faces
      (quote (("TODO" :foreground "red" :weight bold)
	      ("SIGUIENTE" :foreground "yellow" :weight bold)
              ("EN ESPERA" :foreground "blue" :weight bold)
              ("HECHO" :foreground "green" :weight bold)
              ("PROYECTO" :foreground "orange" :weight bold))))

;;Refile
(setq org-refile-targets '(("~/gtd/projects.org" :maxlevel . 3)
                           ("~/gtd/someday.org" :level . 1)
                           ("~/gtd/calendar.org" :maxlevel . 2)))
